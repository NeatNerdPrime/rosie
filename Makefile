# -*- Mode: BSDmakefile; -*-                                          
#
# Makefile for Rosie Pattern Language
#
# © Copyright IBM Corporation 2018.
# LICENSE: MIT License (https://opensource.org/licenses/mit-license.html)
# AUTHOR: Jamie A. Jennings
#
#
# 'make' builds rosie within the current directory.
#    bin/rosie is the rosie command line program.
#    src/librosie/binaries contains librosie.so/dylib and librosie.a.
#    src/librosie/python, src/librosie/go, etc. contain sample librosie clients.
#
# 'make install' builds rosie within the current directory, but
# configures librosie and the CLI to look for rosie files in DESTDIR
# (see default value below).  Then the installation files are copied to:
#    $DESTDIR/lib/librosie.a
#    $DESTDIR/lib/librosie.so (or .dylib on OS X)
#    $DESTDIR/include/librosie.h
#    $DESTDIR/lib/rosie/      (rosie files needed at runtime)
#    $DESTDIR/bin/rosie       (cli)
#
# 'make test' runs all of the blackbox and whitebox tests.
# For the tests to execute, rosie must have been built with as follows:
#    'make LUADEBUG=1'
#

# -----------------------------------------------------------------------------
# Customizable options
# -----------------------------------------------------------------------------

DESTDIR=/usr/local

# -----------------------------------------------------------------------------
# Platform detection
# -----------------------------------------------------------------------------

REPORTED_PLATFORM=$(shell (uname -o || uname -s) 2> /dev/null)
ifeq ($(REPORTED_PLATFORM), Darwin)
PLATFORM=macosx
else ifeq ($(REPORTED_PLATFORM), GNU/Linux)
PLATFORM=linux
else
$(error Unsupported platform (uname reported "$(REPORTED_PLATFORM)"))
endif

# -----------------------------------------------------------------------------
# References to places in the build directory
# -----------------------------------------------------------------------------

ROSIEBIN = $(BUILD_ROOT)/bin/rosie
RPEG_DIR = $(BUILD_ROOT)/src/rpeg
LIBROSIE_DIR = $(BUILD_ROOT)/src/librosie
SUBMOD_DIR = $(BUILD_ROOT)/submodules

LUA_DIR = $(SUBMOD_DIR)/$(LUA)
JSON_DIR = $(SUBMOD_DIR)/$(JSON)
LUAMOD_DIR = $(SUBMOD_DIR)/$(LUAMOD)
READLINE_DIR = $(SUBMOD_DIR)/$(READLINE)

LIBROSIE_A=librosie.a

ifeq ($(PLATFORM),macosx)
PLATFORM=macosx
CC=cc
LIBROSIE_DYLIB=librosie.dylib
else ifeq ($(PLATFORM),linux)
PLATFORM=linux
CC=gcc
LIBROSIE_DYLIB=librosie.so
endif

LIBROSIE_H=librosie.h

# Submodules
ARGPARSE = argparse
LUA = lua
JSON = lua-cjson
READLINE = lua-readline
LUAMOD = lua-modules

BUILD_ROOT = $(shell pwd)

# -----------------------------------------------------------------------------
# Install layout
# -----------------------------------------------------------------------------

LIBROSIED = $(DESTDIR)/lib
ROSIED = $(DESTDIR)/lib/rosie

# Almost everything gets copied to $(ROSIED): 
#   $(ROSIED)/bin          arch-dependent binaries (e.g. rosie, luac)
#   $(ROSIED)/lib          arch-dependent libraries (e.g. *.luac)
#   $(ROSIED)/rpl          standard library (*.rpl)
#   $(ROSIED)/doc          documentation (html format)
#   $(ROSIED)/extra        editor highlighting files, sample docker files, other things
#   $(ROSIED)/CHANGELOG    change log
#   $(ROSIED)/CONTRIBUTORS project contributors, acknowledgements
#   $(ROSIED)/LICENSE      license
#   $(ROSIED)/README       short text readme (e.g. where to open issues)
#   $(ROSIED)/VERSION      installed version
#
# Rosie executable is compiled during 'make install':
#   $(DESTDIR)/bin/rosie
#
# FUTURE: Links into $(ROSIED)
#   $(ROSIE_ROOT)/rpl  --> $(ROSIED)/rpl
#   $(ROSIE_ROOT)/pkg  --> $(ROSIED)/pkg
#   $(ROSIE_DOC)/rosie --> $(ROSIED)/doc

INSTALL_LIB_DIR = $(ROSIED)/lib
INSTALL_RPL_DIR = $(ROSIED)/rpl
INSTALL_DOC_DIR = $(ROSIED)/doc
INSTALL_EXTRA_DIR = $(ROSIED)/extra
INSTALL_MAN_DIR = $(DESTDIR)/share/man/man1
INSTALL_BIN_DIR = $(DESTDIR)/bin
INSTALL_INCLUDE_DIR = $(DESTDIR)/include
INSTALL_ROSIEBIN = $(INSTALL_BIN_DIR)/rosie

# -----------------------------------------------------------------------------
# Targets
# -----------------------------------------------------------------------------

ifdef ROSIE_HOME
ROSIE_HOME_GIVEN_ON_COMMAND_LINE = true
else
ROSIE_HOME="$(shell pwd)"
endif

.PHONY:
.NOTPARALLEL:
default: binaries compile sniff

# <sigh> Once we support packages (like RPM), we won't need this test.
# Note that this test should ALWAYS pass on OS X, since it ships with
# readline.
.PHONY: readlinetest
readlinetest: cctest
	@(bash -c 'printf "#include <stdio.h>\n#include <readline/readline.h>\nint main() { }\n"' | \
	           $(CC) -std=c99 -lreadline -o /dev/null -xc -) && \
	   echo 'libreadline and readline.h appear to be installed' || \
	   (echo '**'; echo '** readline TEST: Missing readline library or readline.h'; echo '**' && /usr/bin/false)

.PHONY: cctest
cctest:
	@(bash -c 'printf "int main() { }\n"' | \
		$(CC) -std=c99 -o  /dev/null -xc -) && \
	echo "C compiler $(CC) appears to be working" || \
	(echo '**'; \
	echo '** C compiler test FAILED. (If on macos, please install XCode Command Line Tools.)'; \
	echo '**' && /usr/bin/false)

# When we compile with gcc, such as on linux, we need bsd/stdio.h to
# compile rpeg.  We check for it here so that we can give a useful
# error message.
.PHONY: libbsdtest
libbsdtest: cctest
	@(bash -c 'printf "#if defined(__linux__)\n#if !defined(__clang__)\n#include <bsd/stdio.h>\n#endif\n#endif\nint main() { }\n"' | \
	           $(CC) -std=c99 -o /dev/null -xc -) && \
	   echo 'bsd/stdio.h appears to be installed (or is not needed)' || \
	   (echo '**'; echo '** libbsd TEST: Missing bsd/stdio.h.  Try running: apt-get install libbsd'; echo '**' && /usr/bin/false)

# The submodule_sentinel indicates that submodules have been
# initialized in the git sense, i.e. that they have been cloned.  The
# sentile file is a file copied from a submodule repo, so that:
# (1) the submodule must have been checked out, and
# (2) the sentinel will not be newer than the submodule files
submodule_sentinel=submodules/~~present~~ 
submodules = submodules/lua/src/Makefile \
		submodules/lua-cjson/Makefile \
		submodules/lua-readline/Makefile
$(submodules): $(submodule_sentinel) readlinetest

.PHONY:
$(submodule_sentinel): 
	if [ -z $$BREW ]; then git submodule init; fi
	if [ -z $$BREW ]; then git submodule update --checkout; fi
	cd $(LUA_DIR) && rm -f include && ln -sf src include
	cp -p $(LUA_DIR)/README $(submodule_sentinel)
	@$(BUILD_ROOT)/src/build_info.sh "git_submodules" $(BUILD_ROOT) "git" >> $(BUILD_ROOT)/build.log

bin/luac: $(LUA_DIR)/src/lua
	mkdir -p bin
	cp $(LUA_DIR)/src/luac bin

$(LUA_DIR)/src/lua: $(submodules)
	$(MAKE) -C "$(LUA_DIR)" CC=$(CC) $(PLATFORM) $(LINUX_CFLAGS) $(LINUX_LDFLAGS)
	@$(BUILD_ROOT)/src/build_info.sh "lua" $(BUILD_ROOT) $(CC) >> $(BUILD_ROOT)/build.log

## ----------------------------------------------------------------------------------------

json_lib=$(JSON_DIR)/cjson.so
lib/cjson.so: $(json_lib) 

$(json_lib): $(submodules) 
	$(MAKE) -C "$(JSON_DIR)" CC=$(CC)
	@$(BUILD_ROOT)/src/build_info.sh "json" $(BUILD_ROOT) $(CC) >> $(BUILD_ROOT)/build.log

lib/argparse.luac: $(submodules) submodules/argparse/src/argparse.lua bin/luac
	bin/luac -o lib/argparse.luac submodules/argparse/src/argparse.lua
	@$(BUILD_ROOT)/src/build_info.sh "argparse" $(BUILD_ROOT) "bin/luac" >> $(BUILD_ROOT)/build.log

readline_lib = $(READLINE_DIR)/readline.so
lib/readline.so: $(readline_lib) 

$(READLINE_DIR)/readline.so: $(submodules)
	$(MAKE) -C "$(READLINE_DIR)" CC=$(CC) LUADIR=../$(LUA)
	@$(BUILD_ROOT)/src/build_info.sh "readline_stub" $(BUILD_ROOT) $(CC) >> $(BUILD_ROOT)/build.log

lib/strict.luac: $(LUAMOD_DIR)/strict.lua bin/luac
	bin/luac -o $@ $<

lib/list.luac: $(LUAMOD_DIR)/list.lua bin/luac
	bin/luac -o $@ $<

lib/thread.luac: $(LUAMOD_DIR)/thread.lua bin/luac
	bin/luac -o $@ $<

lib/recordtype.luac: $(LUAMOD_DIR)/recordtype.lua bin/luac
	bin/luac -o $@ $<

lib/submodule.luac: $(LUAMOD_DIR)/submodule.lua bin/luac
	bin/luac -o $@ $<

lib/%.luac: src/lua/%.lua bin/luac
	@mkdir -p lib
	bin/luac -o $@ $<
	@$(BUILD_ROOT)/src/build_info.sh $@ $(BUILD_ROOT) "bin/luac" >> $(BUILD_ROOT)/build.log

core_objects := $(patsubst src/lua/%.lua,lib/%.luac,$(wildcard src/lua/*.lua))
other_objects := lib/argparse.luac lib/list.luac lib/recordtype.luac lib/submodule.luac lib/strict.luac lib/thread.luac
luaobjects := $(core_objects) $(other_objects)

compile: binaries $(luaobjects) bin/luac $(json_lib) $(readline_lib)

.PHONY:
binaries: libbsdtest $(luaobjects) $(json_lib) $(readline_lib)
	@$(MAKE) -C $(LIBROSIE_DIR) ROSIE_HOME="$(ROSIE_HOME)"
	$(BUILD_ROOT)/src/build_info.sh "binaries" $(BUILD_ROOT) $(CC) >> $(BUILD_ROOT)/build.log

$(ROSIEBIN): binaries $(LIBROSIE_DIR)/binaries/rosie
	cp $(LIBROSIE_DIR)/binaries/rosie "$(BUILD_ROOT)/bin/rosie"

# -----------------------------------------------------------------------------
# Install
# -----------------------------------------------------------------------------

# Main install rule
.PHONY: install installforce 
install:
	@if [ -L "$(ROSIED)" ]; then \
		echo "$(ROSIED) exists and is a symbolic link."; \
		echo "If rosie was installed with 'brew', then run 'brew uninstall rosie'."; \
		echo "Or run 'make installforce' to overwrite the current installation."; \
		exit -1; \
	elif [ -e "$(ROSIED)" ]; then \
		echo "$(ROSIED) already exists. Run 'make installforce' to overwrite it."; \
		exit -1; \
	elif [ -e "$(DESTDIR)/bin/rosie" ]; then \
		echo "$(DESTDIR)/bin/rosie already exists. Run 'make installforce' to overwrite it."; \
		exit -1; \
	else \
		echo "Installing..."; \
		$(MAKE) installforce; \
	fi;

installforce: ROSIE_HOME = "$(DESTDIR)/lib/rosie"
installforce: $(INSTALL_ROSIEBIN) binaries install_metadata install_luac_bin install_rpl install_librosie install_doc install_extras install_man

# We use mv instead of cp for all the binaries, so that the binaries
# will be rebuilt every time "make install" is run, in case DESTDIR
# has changed.

$(INSTALL_ROSIEBIN): compile binaries
	mkdir -p "$(INSTALL_BIN_DIR)"
	-rm -f "$(INSTALL_ROSIEBIN)"
	cp "$(LIBROSIE_DIR)/binaries/rosie" "$(INSTALL_ROSIEBIN)"
	rm -f "$(LIBROSIE_DIR)/binaries/rosie"

# Install librosie
.PHONY: install_librosie
install_librosie: compile binaries
	-rm -f "$(LIBROSIED)/$(LIBROSIE_DYLIB)"
	cp "$(LIBROSIE_DIR)/binaries/$(LIBROSIE_DYLIB)" "$(LIBROSIED)/$(LIBROSIE_DYLIB)"
	rm -f "$(LIBROSIE_DIR)/binaries/$(LIBROSIE_DYLIB)"
	-rm -f "$(LIBROSIED)/$(LIBROSIE_A)"
	cp "$(LIBROSIE_DIR)/binaries/$(LIBROSIE_A)" "$(LIBROSIED)/$(LIBROSIE_A)"
	rm -f "$(LIBROSIE_DIR)/binaries/$(LIBROSIE_A)"
	mkdir -p "$(INSTALL_INCLUDE_DIR)"
	cp "$(LIBROSIE_DIR)/$(LIBROSIE_H)" "$(INSTALL_INCLUDE_DIR)/$(LIBROSIE_H)"

# Install any metadata needed by rosie.  In case $(ROSIED) is a symlink, we try to rm it first.
.PHONY: install_metadata
install_metadata:
	-rm -f "$(ROSIED)"
	mkdir -p "$(ROSIED)"
	cp CHANGELOG CONTRIBUTORS LICENSE README VERSION "$(ROSIED)"
	-cp $(BUILD_ROOT)/build.log "$(ROSIED)"

# Install the lua pre-compiled binary files (.luac)
.PHONY: install_luac_bin
install_luac_bin:
	mkdir -p "$(INSTALL_LIB_DIR)"
	cp lib/*.luac "$(INSTALL_LIB_DIR)"

# TODO: Parameterize this, or use a wildcard
# Install the provided RPL patterns
.PHONY: install_rpl
install_rpl:
	mkdir -p "$(INSTALL_RPL_DIR)"
	cp rpl/*.rpl "$(INSTALL_RPL_DIR)"
	mkdir -p "$(INSTALL_RPL_DIR)"/rosie
	cp rpl/rosie/*.rpl "$(INSTALL_RPL_DIR)"/rosie/
	mkdir -p "$(INSTALL_RPL_DIR)"/builtin
	cp rpl/builtin/*.rpl "$(INSTALL_RPL_DIR)"/builtin/
	mkdir -p "$(INSTALL_RPL_DIR)"/Unicode
	cp rpl/Unicode/*.rpl "$(INSTALL_RPL_DIR)"/Unicode/

.PHONY: install_doc
install_doc:
	mkdir -p "$(INSTALL_DOC_DIR)"
	cp -R doc/ "$(INSTALL_DOC_DIR)"

.PHONY: install_extras
install_extras:
	mkdir -p "$(INSTALL_EXTRA_DIR)"
	cp -R extra/ "$(INSTALL_EXTRA_DIR)"

.PHONY: install_man
install_man:
	mkdir -p "$(INSTALL_MAN_DIR)"
	cp doc/man/rosie.1 "$(INSTALL_MAN_DIR)"

# -----------------------------------------------------------------------------
# Uninstall
# -----------------------------------------------------------------------------

.PHONY: uninstall
uninstall:
	@echo "Removing $(INSTALL_ROSIEBIN)"
	@-rm -vf $(INSTALL_ROSIEBIN)
	@echo "Removing $(ROSIED)"
	@-rm -Rvf $(ROSIED)/
	@echo "Removing librosie.a/.so/.dylib from $(LIBROSIED)"
	@-rm -vf "$(LIBROSIED)/$(LIBROSIE_DYLIB)"
	@-rm -vf "$(LIBROSIED)/$(LIBROSIE_A)"
	@echo "Removing librosie.h from $(INSTALL_INCLUDE_DIR)"
	@-rm -vf "$(INSTALL_INCLUDE_DIR)/$(LIBROSIE_H)" 
	@echo "Removing rosie man page $(INSTALL_MAN_DIR)"
	@-rm -vf "$(INSTALL_MAN_DIR)/rosie.1"

.PHONY: sniff
sniff: $(ROSIEBIN)
	@if [ -n "$(ROSIE_HOME_GIVEN_ON_COMMAND_LINE)" ]; then \
	    echo "Rosie Pattern Engine was built with a custom ROSIE_HOME path: $(ROSIE_HOME)"; \
	    echo "Skipping sniff test of CLI due to custom path."; \
	    true; \
	else \
	RESULT="$(shell $(ROSIEBIN) version 2> /dev/null)"; \
	EXPECTED="$(shell head -1 $(BUILD_ROOT)/VERSION)"; \
	if [ -n "$$RESULT" -a "$$RESULT" = "$$EXPECTED" ]; then \
	    echo "";\
            echo "Rosie Pattern Engine $$RESULT built successfully!"; \
	    if [ -z "$$BREW" ]; then \
	      	    echo "    Use 'make install' to install into DESTDIR=$(DESTDIR)"; \
	      	    echo "    Use 'make uninstall' to uninstall from DESTDIR=$(DESTDIR)"; \
	      	    echo "    To run rosie from the build directory, use ./bin/rosie"; \
	            echo "    Try this example, and look for color text output: rosie match all.things test/resolv.conf"; \
		    echo "";\
	    fi; \
            true; \
        else \
            echo "Rosie Pattern Engine test FAILED."; \
	    echo "    Rosie executable is $(ROSIEBIN)"; \
	    echo "    Expected this output: $$EXPECTED"; \
	    if [ -n "$$RESULT" ]; then \
		echo "    But received this output: $$RESULT"; \
	    else \
		echo "    But received no output to stdout."; \
	    fi; \
	    false; \
        fi; \
	fi

# -----------------------------------------------------------------------------
# Tests need to be done with dumb terminal type because the cli and
# repl tests compare the expected output to the actual output byte by
# byte.  With other terminal types, the ANSI color codes emitted by
# Rosie can be munged by the terminal, making some tests fail when
# they should not.

.PHONY: test
test:
	@$(BUILD_ROOT)/test/rosie-has-debug.sh $(ROSIEBIN) 2>/dev/null; \
	if [ "$$?" -ne "0" ]; then \
	echo "Rosie was not built with LUADEBUG support.  Try 'make clean; make LUADEBUG=1'."; \
	exit -1; \
	fi;
	@echo Running tests in test/all.lua
	@(TERM="dumb"; echo "dofile \"$(BUILD_ROOT)/test/all.lua\"" | $(ROSIEBIN) -D)

.PHONY: clean
clean: libclean
	rm -rf bin/* lib/* 
	-cd $(LUA_DIR) && make clean
	-cd $(JSON_DIR) && make clean
	-cd $(READLINE_DIR) && rm -f readline.so && rm -f src/lua_readline.o
	rm -f build.log

.PHONY: libclean
libclean:
	rm -rf librosie.so librosie.dylib librosie.a
	$(MAKE) -C "$(RPEG_DIR)" clean
	-cd $(LIBROSIE_DIR) && make clean


