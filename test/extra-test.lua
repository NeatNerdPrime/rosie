-- -*- Mode: Lua; -*-                                                               
--
-- extra-test.lua
--
-- © Copyright Jamie A. Jennings 2019.
-- © Copyright IBM Corporation 2016, 2017.
-- LICENSE: MIT License (https://opensource.org/licenses/mit-license.html)
-- AUTHOR: Jamie A. Jennings

assert(TEST_HOME, "TEST_HOME is not set")

test.start(test.current_filename())

list = import("list")
util = import "util"
check = test.check

rosie_cmd = ROSIE_HOME .. "/bin/rosie"
local try = io.open(rosie_cmd, "r")
if try then
   try:close()					    -- found it.  will use it.
else
      error("Cannot find rosie executable")
end
print("Using this rosie executable: " .. rosie_cmd)

testdirs = { ROSIE_HOME .. "/extra/examples" }

for _, dir in ipairs(testdirs) do
   test.heading("Running unit tests on rpl files in " .. dir)
   cmd = rosie_cmd .. " --norcfile test --verbose " .. dir .. "/*.rpl"-- 2>/dev/null"
   print()
   print(cmd)
   results, status, code = util.os_execute_capture(cmd)
   if not results then error("Run failed: " .. tostring(status) .. ", " .. tostring(code)); end
   print(table.concat(results, '\n'))
   if code~=0 then print("Status code was: ", code); end
   check(code==0, "Unit tests failed in " .. dir)
end

return test.finish()
