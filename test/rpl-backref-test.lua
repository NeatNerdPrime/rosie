---- -*- Mode: Lua; -*-                                                                           
----
---- rpl-backref-test.lua
----
---- © Copyright Jamie A. Jennings, 2019.
---- LICENSE: MIT License (https://opensource.org/licenses/mit-license.html)
---- AUTHOR: Jamie A. Jennings

assert(TEST_HOME, "TEST_HOME is not set")

test.start(test.current_filename())

list = import("list")
util = import "util"
violation = import "violation"
check = test.check

rosie_cmd = ROSIE_HOME .. "/bin/rosie"
local try = io.open(rosie_cmd, "r")
if try then
   try:close()					    -- found it.  will use it.
else
      error("Cannot find rosie executable")
end
print("Using this rosie executable: " .. rosie_cmd)

testdir = ROSIE_HOME .. "/test"

fn = "backref-rpl.rpl"

----------------------------------------------------------------------------------------
test.heading("Running unit-tests on " .. fn)

cmd = rosie_cmd .. " --norcfile test --verbose " .. testdir .. "/" .. fn-- 2>/dev/null"
print()
print(cmd)
results, status, code = util.os_execute_capture(cmd)
if not results then error("Run failed: " .. tostring(status) .. ", " .. tostring(code)); end
print(table.concat(results, '\n'))
if code~=0 then print("Status code was: ", code); end
check(code==0, "Unit tests failed on " .. fn)

----------------------------------------------------------------------------------------
test.heading("Misc backref tests")

check(type(rosie)=="table")
e = rosie.engine.new()
check(e)

ok, _, errs = e:load('foo = backref:bar')
check(not ok)
check(type(errs)=="table")
check(type(errs[1])=="table")
str = violation.tostring(errs[1])
check(str:find("undefined identifier: bar", 1, true))

ok, pkg, _ = e:load('package qux bar = "hi"')
assert(pkg == "qux")

ok, _, errs = e:load('foo = backref:qux.bar')
check(ok)

p, errs = e:compile('qux.bar foo')
assert(p)
m, leftover = p:match('hi hi')
check(m); check(leftover==0)

------------------------------------------------------------------

ok, pkg, _ = e:load('package qux local bar = "hi"')
assert(pkg == "qux")

ok, _, errs = e:load('foo = backref:qux.bar')
check(not ok)
str = violation.tostring(errs[1])
check(str:find("undefined identifier: qux.bar", 1, true))

------------------------------------------------------------------

ok, _, errs = e:load('foo = backref:foo')
check(not ok)
check(type(errs)=="table")
check(type(errs[1])=="table")
str = violation.tostring(errs[1])
check(str:find("identifier being defined cannot be referenced on right hand side", 1, true))

-- The . pattern does not capture anything, so any backref to . will never match anything.
ok, _, errs = e:load('foo = backref:.')
check(ok)

ok, _, errs = e:load('foo_plus = foo+')
check(ok)
p = e:compile('foo_plus')
check(p)
m, leftover = p:match('a')
check(not m)
check(leftover == 1)

p = e:compile('{. foo_plus}')
m, leftover = p:match('a')
check(not m)
m, leftover = p:match('ab')
check(not m)
m, leftover = p:match('aa')
check(not m)

-- Define 'any' to capture what . matches (a single char).
ok, _, errs = e:load('any = .; foo = backref:any; foo_plus = foo+; foo_star = foo*')
check(ok)

p = e:compile('foo_plus')
check(p)
m, leftover = p:match('a')
check(not m)

p = e:compile('{any foo_star}')
check(p)
m, leftover = p:match('a')
check(m and (leftover==0))
m, leftover = p:match('aa')
check(m and (leftover==0))
m, leftover = p:match('aaa')
check(m and (leftover==0))

p = e:compile('{any foo_plus}')
check(p)
m, leftover = p:match('a')
check(not m)
m, leftover = p:match('aa')
check(m and (leftover==0))
m, leftover = p:match('aaa')
check(m and (leftover==0))

p = e:compile('{any foo foo}')
check(p)
m, leftover = p:match('a')
check(not m)
m, leftover = p:match('aa')
check(not m)
m, leftover = p:match('aaa')
check(m and (leftover==0))

p = e:compile('{any foo_plus}')
m, leftover = p:match('a')
check(not m)
m, leftover = p:match('ab')
check(not m)
m, leftover = p:match('aa')
check(m and (leftover == 0))
m, leftover = p:match('aaaaaaaa')
check(m and (leftover == 0))

m, leftover = p:match('aaaaaaaaK')
check(m and (leftover == 1))

p, errs = e:compile('backref:any')
check(p)

p, errs = e:compile('backref:any+')
check(not p)
check(violation.tostring(errs[1]):find('is not the name of a pattern'))

p, errs = e:compile('backref:{any}+')
check(not p)
check(violation.tostring(errs[1]):find('is not the name of a pattern'))

p, errs = e:compile('backref:{{any}}')
check(not p)
check(violation.tostring(errs[1]):find('is not the name of a pattern'))

p, errs = e:compile('backref:{any}')
check(not p)
check(violation.tostring(errs[1]):find('is not the name of a pattern'))

p, errs = e:compile('backref:(any)')
check(not p)
check(violation.tostring(errs[1]):find('is not the name of a pattern'))

p, errs = e:compile('{backref:any}+')
check(p)

m, leftover = p:match('abc')
check(not m)

p, errs = e:compile('any {backref:any}+')
check(p)

m, leftover = p:match('a a')
check(m and (leftover == 0))
m, leftover = p:match('a b')
check(not m)
m, leftover = p:match('aa')
check(not m)

m, leftover = p:match('a a')
check(m and (leftover == 0))
m, leftover = p:match('a b')
check(not m)
m, leftover = p:match('aa')
check(not m)

p, errs = e:compile('backref:xyz')
check(not p)
check(violation.tostring(errs[1]):find('undefined identifier: xyz', 1, true))

return test.finish()
